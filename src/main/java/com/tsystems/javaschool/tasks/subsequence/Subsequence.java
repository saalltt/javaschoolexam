package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here

        //for test 8,9
        if (x == null || y == null) throw new IllegalArgumentException();

        //for test 5,6
        if (x.isEmpty()) return true;

        int i = 0;
        ArrayList<Object> list = new ArrayList<>();
        for (Object element : y) {
            if (element.equals(x.get(i))) {
                list.add(element);
                i++;
                if (i == x.size()) break;
            }
        }
        if (list.size() == x.size()) return true;
        return false;
    }

}
