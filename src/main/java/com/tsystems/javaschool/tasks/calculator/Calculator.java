package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public static void main(String[] args) {
      Calculator calculator = new Calculator();
      System.out.println(calculator.evaluate("3+2"));
    }

    public static String toRpn(String ins) {
        String ours = "";
        Stack<Character> stack = new Stack<>();
        int prior;
        int count = 0;
        int count2 = 0;
        try {
            for (int i = 0; i < ins.length(); i++) {
                prior = priority(ins.charAt(i));
                if (ins.charAt(i) == ',') {
                    return null;
                }
                if (prior == 0)
                    ours = ours + ins.charAt(i);
                if (prior == 1) {
                    stack.push(ins.charAt(i));
                    count++;
                }
                if (prior > 1) {
                    ours += ' ';
                    while (!stack.empty()) {
                        if (priority(stack.peek()) >= prior)
                            ours += stack.pop();
                        else break;
                    }
                    stack.push(ins.charAt(i));
                }
                if (prior == -1) {
                    ours += ' ';
                    while (priority(stack.peek()) != 1) {
                        ours = ours + stack.pop();
                    }
                    stack.pop();
                    count2++;
                }
            }
            if (count > count2 && count2 < count) return null;
            while (!stack.empty())
                ours += stack.pop();
            return ours;
        } catch (Exception e) {
            return null;
        }
    }

    public static double outRpn(String outs) {
        String ours = "";
        Stack<Double> stack = new Stack<>();
        try {
            if (outs.equals("incorrect") && outs.equals("")) {
                return 404;
            }
            for (int i = 0; i < outs.length(); i++) {
                if (outs.charAt(i) == ' ') continue;
                if (priority(outs.charAt(i)) == 0) {
                    while (outs.charAt(i) != ' ' && priority(outs.charAt(i)) == 0) {
                        ours += outs.charAt(i++);
                        if (i == outs.length())
                            break;
                    }
                    stack.push(Double.parseDouble(ours));
                    ours = "";
                }
                if (priority(outs.charAt(i)) > 1) {
                    double a = stack.pop();
                    double b = stack.pop();
                    if (outs.charAt(i) == '+')
                        stack.push(a + b);
                    if (outs.charAt(i) == '-')
                        stack.push(b - a);
                    if (outs.charAt(i) == '*')
                        stack.push(a * b);
                    if (outs.charAt(i) == '/')
                        stack.push(b / a);
                    if (a == 0) {
                        return 404;
                    }
                }
            }
            return stack.pop();
        } catch (Exception e) {
            return 404;
        }
    }

    public static String evaluate(String s) {
        if (outRpn(toRpn(s)) == 404) {
            return null;
        }
        DecimalFormatSymbols df = new DecimalFormatSymbols(Locale.US);
        DecimalFormat df1 = new DecimalFormat("#.####", df);
        return String.valueOf(df1.format(outRpn(toRpn(s))));
    }

    public static int priority(char c) {
        if (c == '*' || c == '/')
            return 3;
        else if (c == '-' || c == '+')
            return 2;
        else if (c == '(')
            return 1;
        else if (c == ')')
            return -1;
        else return 0;
    }
}


