package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        int row = getRow(inputNumbers.size()) - 1;
        int col = row * 2 - 1;
        int[][] arr = new int[row][col];

        Collections.sort(inputNumbers);
        int getIndex = 0;
        int set1;
        int set2;

        for (int i = 0; i < row; i++) {
            set1 = (col - (2 * i + 1)) / 2;
            set2 = col - set1;
            for (int j = set1; j < set2; j = j + 2) {
                arr[i][j] = inputNumbers.get(getIndex);
                getIndex++;
            }
        }
        return arr;
    }

    public static int getRow(int size) {
        int count = 0;
        while (size > 0) {
            size = size - count;
            count++;
        }
        if (size < 0) {
            throw new CannotBuildPyramidException();
        }
        return count;

    }



}
